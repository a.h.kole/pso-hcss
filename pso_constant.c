// Reinier is lazy, copy this to run
// /usr/local/opt/llvm/bin/clang -fopenmp -L/usr/local/opt/llvm/lib -Wall -O3 pso_constant.c hcss-npt-func.c sann.c -o pso_constant.out -lgsl -I/usr/local/include -L/usr/local/lib

#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <unistd.h>
#include "inverse_crystal_design.h"

#define NPAR 2
#define NBIRDS 25

// Parameters
const double c1 = 2;
const double c2 = 2;
int k = 6;


// Variables
double p[NBIRDS][NPAR];
double v[NBIRDS][NPAR];
double f[NBIRDS];

int n_particles;
double r[NBIRDS][N][NDIM];
double box[NBIRDS][NDIM];
gsl_rng *RNGS[NBIRDS];

double p_best[NBIRDS][NPAR];
double f_best[NBIRDS];

double p_best_global[NPAR];
double f_best_global = 1;


void init_pso(const char *init_filename, gsl_rng *RNG){

    // only read data once, and after 
	read_data(r[0], &n_particles, box[0], init_filename);

	for(int i = 0; i < NBIRDS; i++){

		if(i!=0){

			// copy init box size to all birds
			for(int d = 0; d < NDIM; d++){
				box[i][d] = box[0][d];
			}

			// copy init particle positions to all birds
			for(int n = 0; n < n_particles; n++){
				for(int d = 0; d < NDIM; d++){
					r[i][n][d] = r[0][n][d];
				}
			}
		}

		// Start in the fluid phase, i.e. 0.25 <= T <= 0.50 and 5 <= betaP <= 18

		// init reduced temperature
		p[i][0] = .20 + .30 * gsl_rng_uniform(RNG);
		p_best[i][0] = p[i][0];
		v[i][0] = .01 * (2 * gsl_rng_uniform(RNG) - 1);

		// init reduced pressure
		p[i][1] = 5 + 13 * gsl_rng_uniform(RNG);
		p_best[i][1] = p[i][1];
		v[i][1] = 1 * (2 * gsl_rng_uniform(RNG) - 1);
	
		// init best fitness
		f_best[i] = 1;

		// Initialise RNGs
		RNGS[i] = gsl_rng_alloc(gsl_rng_mt19937);
		gsl_rng_set(RNGS[i], i * time(NULL));
	}
}

int main(int argc, char* argv[]){

	// Initialization variables 
	const char* init_filename = "xy.dat";

	// init random number generator
    gsl_rng *RNG = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(RNG, time(NULL));

    // init birds
    init_pso(init_filename, RNG);

    // setup output file
    char buffer[128];
    time_t rawtime;
    struct tm *loctime;
    time(&rawtime);
    loctime = localtime(&rawtime);

    strftime(buffer, 128, "pso_results_%d-%m-%y_%H%M%S.dat", loctime);
    FILE* fp = fopen(buffer, "w");

    fprintf(fp, "bond order target (k): %d\n", k);
    fprintf(fp, "step no. \t arg_1 \t arg_2 \t fitness\n");

    fclose(fp);

    // start iterations
    for(int step = 0; step < 100; step++){

    	printf("starting iteration: %d\n", step);

		// reset locations
		for(int j = 0; j < NBIRDS; j++){
			read_data(r[j], &n_particles, box[j], init_filename);
		};

    	// evaluate fitness function for every bird
    	#pragma omp parallel
    	{
    		#pragma omp for
    		for(int j = 0; j < NBIRDS; j++){
    			double beta = 1 / p[j][0];
    			double P = p[j][1] * p[j][0];
    			double bond_order = hcss_npt(r[j], n_particles, box[j], beta, P, 1.4, k, RNGS[j]);
    			
                f[j] = (1 - bond_order) * (1 - bond_order);
    		}
    	}

        // append to output file
        FILE* fp = fopen(buffer, "a");
        for(int j = 0; j < NBIRDS; j++){
            fprintf(fp, "%d\t%lf\t%lf\t%lf\n", step, p[j][0], p[j][1], f[j]);
        }
        fclose(fp);

    	printf("setting new bests\n");
    	// set new bests
    	for(int j = 0; j < NBIRDS; j++){
    		
    		// personal bests
    		if(f[j] < f_best[j]){
    			f_best[j] = f[j];
    			for(int d = 0; d < NPAR; d++){
    				p_best[j][d] = p[j][d];
    			}
    		}
    		// global best
    		if(f[j] < f_best_global){
    			f_best_global = f[j];
    			for(int d = 0; d < NPAR; d++){
    				p_best_global[d] = p[j][d];
    			}
    		}
    	}
    	printf("best global position (fitness): %lf\t%lf\t%lf\n", p_best_global[0], p_best_global[1], f_best_global);

    	printf("setting new positions\n");
    	// set new positions
    	for(int j = 0; j < NBIRDS; j++){
    		for(int d = 0; d < NPAR; d++){
    			v[j][d] = v[j][d] + c1 * gsl_rng_uniform(RNG) * (p_best[j][d] - p[j][d])
    							  + c2 * gsl_rng_uniform(RNG) * (p_best_global[d] - p[j][d]);

    			p[j][d] = p[j][d] + v[j][d];
				}

            // Check to make sure that parameters are within bounds of phase diagram

            // Note that we want crystals, i.e. no liquids or gases, so keep temperature below at least .. 
            if(p[j][0] < 0.05){
                p[j][0] = 0.05;
    		}
            if(p[j][0] > 0.55){
                p[j][0] = 0.55;
            }

            // Note that we want crystals, i.e. no liquids or gases, so keep pressure above at least .. 
            if(p[j][1] < 1.0){
                p[j][1] = 1.0;
            }
            if(p[j][1] > 60.0){
                p[j][1] = 60.0;
            }
    	}
    }

    printf("best global position (fitness): %lf\t%lf\t%lf\n", p_best_global[0], p_best_global[1], f_best_global);
    return 0;
}
