#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <mpi.h>
#include <unistd.h>
#include "inverse_crystal_design.h"

#define NPAR 2
#define NBIRDS 24
#define NPROC 8

// Parameters
const double c1 = 2;
const double c2 = 2;
int k = 4;


// Variables
double p[NBIRDS / NPROC * NPAR];
double v[NBIRDS / NPROC * NPAR];
double f[NBIRDS / NPROC];

double p_collect[NBIRDS * NPAR];
double v_collect[NBIRDS * NPAR];
double f_collect[NBIRDS];

int n_particles;
double r[NBIRDS / NPROC][N][NDIM];
double box[NBIRDS / NPROC][NDIM];

double r_init[N][NDIM];
double box_init[NDIM];

double p_best[NBIRDS / NPROC * NPAR];
double f_best[NBIRDS / NPROC];

double p_best_global[NPAR];
double f_best_global = 1;

void init_pso(const char *init_filename, gsl_rng *RNG){

    // only read data once, and after 
	read_data(r_init, &n_particles, box_init, init_filename);

	for(int i = 0; i < NBIRDS / NPROC; i++){

		// copy init box size to all birds
		for(int d = 0; d < NDIM; d++){
			box[i][d] = box_init[d];
		}

		// copy init particle positions to all birds
		for(int n = 0; n < n_particles; n++){
			for(int d = 0; d < NDIM; d++){
				r[i][n][d] = r_init[n][d];
			}
		}

		// Start in the fluid phase, i.e. 0.25 <= T <= 0.50 and 5 <= betaP <= 18

		// init reduced temperature
		p[i * NPAR] = .25 + .25 * gsl_rng_uniform(RNG);
		p_best[i * NPAR] = p[i * NPAR];
		v[i * NPAR] = .01 * (2 * gsl_rng_uniform(RNG) - 1);

		// init reduced pressure
		p[i * NPAR + 1] = 5 + 13 * gsl_rng_uniform(RNG);
		p_best[i * NPAR + 1] = p[i * NPAR + 1];
		v[i * NPAR + 1] = 1 * (2 * gsl_rng_uniform(RNG) - 1);
	
		// init best fitness
		f_best[i] = 1;
	}
}

void reset_pos(void){
    for(int i = 0; i < NBIRDS / NPROC; i++){
        for(int d = 0; d < NDIM; d++){
             box[i][d] = box_init[d];
        }

        for(int n = 0; n < n_particles; n++){
               for(int d = 0; d < NDIM; d++){
                       r[i][n][d] = r_init[n][d];
               }
        }
    }
}

int main(int argc, char* argv[]){

	// Initialise MPI
	MPI_Init(NULL, NULL);

	// Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	assert(world_size == NPROC);

	// Initialization variables 
	const char* init_filename = "xy_fluid.dat";

	// init random number generator
    gsl_rng *RNG = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(RNG, world_rank * time(NULL));

    // init birds
    init_pso(init_filename, RNG);

	char buffer[128];
	if(world_rank == 0){
    	// setup output file
    	time_t rawtime;
    	struct tm *loctime;
    	time(&rawtime);
    	loctime = localtime(&rawtime);

    	strftime(buffer, 128, "pso_results_%d-%m-%y_%H%M%S.dat", loctime);
    	FILE* fp = fopen(buffer, "w");

    	fprintf(fp, "bond order target (k): %d\n", k);
    	fprintf(fp, "step no. \t arg_1 \t arg_2 \t v_1 \t v_2 \t fitness\n");

    	fclose(fp);
	}

    // start iterations
    for(int step = 0; step < 25; step++){
		if(world_rank == 0){
    		printf("starting iteration: %d\n", step);
		}

    	// evaluate fitness function for every bird
    	for(int j = 0; j < NBIRDS / NPROC; j++){
    		double beta = 1 / p[j * NPAR];
    		double P = p[j * NPAR + 1] * p[j * NPAR];
    		double bond_order = hcss_npt(r[j], n_particles, box[j], beta, P, 1.4, k, RNG);
    		
            f[j] = (1 - bond_order) * (1 - bond_order);
    	}

        // Reset positions of particles for next generation
        reset_pos();

        // Apply penalty if position out of bounds
        for(int j = 0; j < NBIRDS / NPROC; j++){
            if(p[j * NPAR] < 0.05){
                // f[j] *= 1 + (0.05 - p[j * NPAR]) / 0.05;
                f[j] += 2;
            }
            if(p[j * NPAR] > 0.55){
                // f[j] *= 1 + (p[j * NPAR] - 0.55) / 0.55;
                f[j] += 2;
            }

            if(p[j * NPAR + 1] < 5.0){
                // f[j] *= 1 + (5.0 - p[j * NPAR + 1]) / 5.0;
                f[j] += 2;
            }
            if(p[j * NPAR + 1] > 60.0){
                // f[j] *= 1 + (p[j * NPAR + 1] - 60.0) / 60.0;
                f[j] += 2;
            }
        }

		// Collect information in root process
		MPI_Gather(f, NBIRDS / NPROC,        MPI_DOUBLE, f_collect, NBIRDS / NPROC,        MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Gather(p, NBIRDS / NPROC * NPAR, MPI_DOUBLE, p_collect, NBIRDS / NPROC * NPAR, MPI_DOUBLE, 0, MPI_COMM_WORLD);
                MPI_Gather(v, NBIRDS / NPROC * NPAR, MPI_DOUBLE, v_collect, NBIRDS / NPROC * NPAR, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        // append to output file
		if(world_rank == 0){
        	FILE* fp = fopen(buffer, "a");
        	for(int j = 0; j < NBIRDS; j++){
        	    fprintf(fp, "%d\t%lf\t%lf\t%lf\t%lf\t%lf\n", step, p_collect[j * NPAR], p_collect[j * NPAR + 1], v_collect[j * NPAR], v_collect[j * NPAR + 1], f_collect[j]);
        	}
        	fclose(fp);
		}

		if(world_rank == 0){
    		printf("setting new bests\n");
		}
    	// set new personal bests
    	for(int j = 0; j < NBIRDS / NPROC; j++){
    		
    		// personal bests
    		if(f[j] < f_best[j]){
    			f_best[j] = f[j];
    			for(int d = 0; d < NPAR; d++){
    				p_best[j * NPAR + d] = p[j * NPAR + d];
    			}
    		}
    	}

		// set new global bests
		if(world_rank == 0){
			for(int j = 0; j < NBIRDS; j++){
				// global best
    			if(f_collect[j] < f_best_global){
    				f_best_global = f_collect[j];
    				for(int d = 0; d < NPAR; d++){
    					p_best_global[d] = p_collect[j * NPAR + d];
    				}
    			}
			}
		}

		// Send global bests back to all processes
		MPI_Bcast(&f_best_global, 1,    MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Bcast(p_best_global,  NPAR, MPI_DOUBLE, 0, MPI_COMM_WORLD);

		if(world_rank == 0){
    		printf("best global position (fitness): %lf\t%lf\t%lf\n", p_best_global[0], p_best_global[1], f_best_global);

    		printf("setting new positions\n");
		}
    	// set new positions
    	for(int j = 0; j < NBIRDS / NPROC; j++){
    		for(int d = 0; d < NPAR; d++){
    			v[j * NPAR + d] = v[j * NPAR + d] + c1 * gsl_rng_uniform(RNG) * (p_best[j * NPAR + d] - p[j * NPAR + d])
    							  				  + c2 * gsl_rng_uniform(RNG) * (p_best_global[d]     - p[j * NPAR + d]);
    			p[j * NPAR + d] = p[j * NPAR + d] + v[j * NPAR + d];
			}

            // Check to make sure that parameters are within bounds of phase diagram

            // Note that we want crystals, i.e. no liquids or gases, so keep temperature below at least .. 
            if(p[j * NPAR] <= 0){
                p[j * NPAR] = 0.01;
    		}

            // Note that we want crystals, i.e. no liquids or gases, so keep pressure above at least .. 
            if(p[j * NPAR + 1] <= 0){
                p[j * NPAR + 1] = 1.0;
            }
    	}
    }

	if(world_rank == 0){
    	printf("best global position (fitness): %lf\t%lf\t%lf\n", p_best_global[0], p_best_global[1], f_best_global);
	}

	// Finalize the MPI environment.
    MPI_Finalize();

    return 0;
}
