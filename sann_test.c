#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include "inverse_crystal_design.h"

const char* init_filename = "xy_FL.dat";

int n_particles = 0;
double r[N][NDIM];
double box[NDIM];

typedef struct{
    double re;
    double im;
}complex_number;

void read_data(double r[][NDIM], int *n_particles, double box[], const char *init_filename){
    // Initialise the file pointer
    FILE *fp;
    fp = fopen(init_filename, "r");

    // First read in the number of particles and box dimension
    fscanf(fp, "%d", n_particles);
    for(int d = 0; d < NDIM; d++){
        double low, high;
        fscanf(fp, "%lf %lf", &low, &high);
        box[d] = high - low;
    }

    // Read in all the particle positions
    for(int i = 0; i < *n_particles; i++){
        for(int d = 0; d < NDIM; d++){
            fscanf(fp, "%lf", &r[i][d]);
        }
        fscanf(fp, "%*f"); // Ignore the DIAMETER field
    }
}

void calculate_bond_order_exp(double x, double y, double r, int k, complex_number *chi){

    double rn;
    switch(k){
        case 2:
            rn = r * r;
            (*chi).re += (x - y) * (x + y) / rn ;
            (*chi).im += 2 * x * y / rn ;
            break;
        case 3:
            rn = r * r * r;
            (*chi).re += ( x * x * x - 3 * x * y * y ) / rn;
            (*chi).im += ( 3 * x * x * y - y * y * y )/ rn;
            break;
        case 4:
            rn = r * r * r * r;
            (*chi).re += ( x * x * x * x - 6 * x * x * y * y + y * y * y * y) / rn;
            (*chi).im += 4 * x * (x - y) * y * (x + y) / rn;
            break;
        case 5:
            rn = r * r * r * r * r;
            (*chi).re += ( x * x * x * x * x - 10 * x * x * x * y * y + 5 * x * y * y * y * y) / rn;
            (*chi).im += ( 5 * x * x * x * x * y - 10 * x * x * y * y * y + y * y * y * y * y) / rn;
            break;
        case 6:
            rn = r * r * r * r * r * r;
            (*chi).re += ( x * x * x * x * x * x - 15 * x * x * x * x * y * y + 15 * x * x * y * y * y * y - y * y * y * y * y * y) / rn;
            (*chi).im += ( 6 * x * x * x * x * x * y - 20 * x * x * x * y * y * y + 6 * x * y * y * y * y * y) / rn;
            break;
    }
}

double calculate_bond_order(double r[][NDIM], int n_particles, double box[], nn_info nn[][N], int m_nn[], int k){

    assert(NDIM == 2);

    double chi_tot = 0;
    for(int i = 0; i < n_particles; i++){

        complex_number chi;
        chi.re = 0;
        chi.im = 0;

        for(int j = 1; j <= m_nn[i]; j++){

            double min_x = r[nn[i][j].index][0] - r[i][0];
            min_x -= (int)(2.0 * min_x / box[0]) * box[0];

            double min_y = r[nn[i][j].index][1] - r[i][1];
            min_y -= (int)(2.0 * min_y / box[1]) * box[1];

            calculate_bond_order_exp(min_x, min_y,  *(nn[i][j].dist), k, &chi);
        }

        chi_tot += (chi.re * chi.re + chi.im * chi.im) / (m_nn[i] * m_nn[i]);
    }

    return chi_tot / n_particles;
}


int main(int argc, char* argv[]){

    read_data(r, &n_particles, box, init_filename);

    double dist[N][N];
    nn_info nn[N][N];
    int m_nn[N];
    double bond_order[5];

    initialise_sann(r, dist, nn, n_particles, box);

    sann(r, dist, nn, m_nn, n_particles);
    for(int k = 2; k <= 6; k++){
            bond_order[k - 2] = calculate_bond_order(r, n_particles, box, nn, m_nn, k);
    }

    for( int k = 0; k < 5; k++){
        printf("bond order %d: \t %lf\n", k + 2, bond_order[k]);
    }

    // for(int i = 0; i < n_particles; i++){
    //     printf("%d\n", m_nn[i]);
    // }

    // for(int i = 0; i < n_particles; i++){
    //     printf("%lf\n", *(nn[3][i].dist));
    // }

    return 0;
}
