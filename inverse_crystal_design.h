#include <math.h>
#include <gsl/gsl_rng.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef NDIM
#define NDIM 2
#endif

#ifndef N
#define N 300
#endif

#ifndef DIAMETER
#define DIAMETER 1.
#endif

typedef struct{
    int index; // index of particle
    double *dist; // distance to this particle (pointer into dist array)
}nn_info;

// Initialises all arrays used for the nn algorithm
void initialise_sann(double r[][NDIM], double dist[][N], nn_info nn[][N], int n_particles, double box[]);

// Computes all distances between all pairs of particles.
// The positions of the particles are stored in the array r.
// The resulting distances are stored in the array dist.
// dist[i][j] will represent the distance between particles
// i and j.
void compute_distances(double r[][NDIM], double dist[][N], int n_particles, double box[]);

// Computes the changes to the distance array when the particle
// with index pid has been moved.
void update_distance_particle_move(double r[][NDIM], double dist[][N], int n_particles, double box[], int pid);

// Updates the dist array after the volume has been changed with factor scale_factor.
// Note that a volume change cannot change the relative ordering of particle distances,
// so the nn array does not have to be updated.
void update_distance_volume_change(double dist[][N], int n_particles, double scale_factor);

// Computes for each particle the amount of nearest neighbors, according to the SANN algorithm, 
// the results are written in the m_nn array. 
void sann(double r[][NDIM], double dist[][N], nn_info nn[][N], int m_nn[], int n_particles);

// Runs hcss npt monte carlo simulation and writes the resulting bond order parameters in bond_order_res. 
double hcss_npt(double r[][NDIM], int n_particles, double box[], const double beta, const double P, const double lambda, int k, gsl_rng *RNG);

//
void read_data(double r[][NDIM], int *n_particles, double box[], const char *init_filename);

//
void write_data(int step, double r[][NDIM], int n_particles, double box[]);