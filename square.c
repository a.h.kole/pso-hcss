#include <stdio.h>
#include <time.h>
#include <math.h>
#include "mt19937.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define DIAM 1.

int main(int argc, char *argv[]){

	int n; // total number of particles in one direction
	double f; // packing fraction

	if(argc != 3){
		printf("Error: %d arguments given, expected 2\n", argc - 1);
		return 1;
	}
	else{
		sscanf(argv[1], "%d", &n);
		sscanf(argv[2], "%lf", &f);
	}

	double r = DIAM / 2; // particle radius
	int N = n * n; // total number of unit cells

	double v = (r * r) * M_PI / f; // unitcell area
	double l = sqrt(v); // unitcell length

	double V = N * v; // box volume
	double L = sqrt(V); // box length
	
	FILE *fp;

	fp = fopen("xy.dat", "w");
	fprintf(fp, "%d\n", N);

	fprintf(fp, "%lf %lf\n", 0., L);
	fprintf(fp, "%lf %lf\n", 0., L);

	for(int nx = 0; nx < n; nx++){
		for(int ny = 0; ny < n; ny++){
				fprintf(fp, "%lf %lf %lf\n", nx * l, ny * l, DIAM);
		}
	}

	fclose(fp);

	return 0;
}