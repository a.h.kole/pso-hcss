#include <stdlib.h>
#include "inverse_crystal_design.h"


// Comparison function for nn_info struct.
// These structs are compared based on the value
// stored in the dist pointer.
int compare_nn_info(const void *a, const void *b){
    const nn_info *sa = (const nn_info *) a;
    const nn_info *sb = (const nn_info *) b;

    const double da     = *((*sa).dist);
    const double db     = *((*sb).dist);

    return (da > db) - (da < db);
}

// Initialises all arrays used for the nn algorithm
void initialise_sann(double r[][NDIM], double dist[][N], nn_info nn[][N], int n_particles, double box[]){
    // Set all diagonal elements to 0 (distance to yourself is always 0)
    for(int i = 0; i < n_particles; i++){
        dist[i][i] = 0;
    }
    
    compute_distances(r, dist, n_particles, box);

    // Initialise nn_info array
    for(int i = 0; i < n_particles; i++){
        for(int j = 0; j < n_particles; j++){
            nn[i][j].index = j;
            nn[i][j].dist  = &dist[i][j];
        }
    }
}

// Computes all distances between all pairs of particles.
// The positions of the particles are stored in the array r.
// The resulting distances are stored in the array dist.
// dist[i][j] will represent the distance between particles
// i and j.
void compute_distances(double r[][NDIM], double dist[][N], int n_particles, double box[]){
    for(int i = 0; i < n_particles - 1; i++){
        for(int j = i + 1; j < n_particles; j++){
            double dst2 = 0;
            for(int d = 0; d < NDIM; ++d){
                double min_d = r[i][d] - r[j][d];
                // Find the distance with the Nearest Image Convention
                min_d -= (int)(2.0 * min_d / box[d]) * box[d];

                dst2 += min_d * min_d;
            }
            dist[i][j] = sqrt(dst2);
            dist[j][i] = dist[i][j]; 
        }
    }
}

// Computes the changes to the distance array when the particle
// with index pid has been moved.
void update_distance_particle_move(double r[][NDIM], double dist[][N], int n_particles, double box[], int pid){
    for(int j = 0; j < n_particles; j++){
        if(j == pid) continue;
        double dst2 = 0;
        for(int d = 0; d < NDIM; ++d){
            double min_d = r[pid][d] - r[j][d];
            // Find the distance with the Nearest Image Convention
            min_d -= (int)(2.0 * min_d / box[d]) * box[d];

            dst2 += min_d * min_d;
        }
        dist[pid][j] = sqrt(dst2);
        dist[j][pid] = dist[pid][j]; 
    }
}


// Updates the dist array after the volume has been changed with factor scale_factor.
// Note that a volume change cannot change the relative ordering of particle distances,
// so the nn array does not have to be updated.
void update_distance_volume_change(double dist[][N], int n_particles, double scale_factor){
    for(int i = 0; i < n_particles - 1; i++){
        for(int j = i + 1; j < n_particles; j++){
            dist[i][j] *= scale_factor;
            dist[j][i] = dist[i][j];
        }
    }
}

// 
void sann(double r[][NDIM], double dist[][N], nn_info nn[][N], int m_nn[], int n_particles){

    // Sort the nn_info array
    for(int i = 0; i < n_particles; i++){
        qsort(nn[i], n_particles, sizeof(nn_info), compare_nn_info);
    }

    // Find neighbours according to SANN algorithm
    for(int i = 0; i < n_particles; i++){
        for(int m = 3; m < n_particles - 1; m++){
            double R = 0;
            for(int j = 1; j <= m; j++){
                R += *(nn[i][j].dist);
            }
            R /= 0.5 * M_PI * (m - 2);

            if(R < *(nn[i][m+1].dist)){
                m_nn[i] = m;
                break;
            }
        }
    }
}

