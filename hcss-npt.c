// Reinier is lazy, copy this
// gcc -Wall -O3 hcss-npt.c sann.c -o hcss-npt.out -lgsl


#include <stdio.h>
#include <time.h>
#include <assert.h>
#include "inverse_crystal_design.h"

/* Functions */
typedef struct{
    int no_overlap;
    double energy;
}particle_overlap_energy;

typedef struct{
	double re;
	double im;
}complex_number;

/* Function that computes the mean of the values in an array
Parameters:
    1. data: array containing values
    2. size: number of values in the array
*/
double mean(double data[], int size){
    double avg = 0;
    for(int i = 0; i < size; i++){
        avg += data[i];
    }
    return avg / size;
}

/* Function that computes the mean squared error of the values in an array
   given the mean.
Parameters:
    1. data: array containing values
    2. size: number of values in array
    3. mean: mean of values in array (so it doesn't have to be computed again)
*/
double msd(double data[], int size, double avg){
    double deviation = 0;
    for(int i = 0; i < size; i++){
        deviation += (data[i] - avg) * (data[i] - avg);
    }
    return deviation / size;
}

// Return 0 if particle pid overlaps with particle j, else 1 if no overlap 
int check_overlap(double dist[][N], int n_particles, double lambda, int pid, int j, double* enew){

    for(int i = j; i < n_particles; ++i){
        if(i == pid) continue;

        if(dist[pid][i] <= DIAMETER){
            //particle is overlapping, so reject the move
            return 0;
        }
        else if(dist[pid][i] <= DIAMETER * lambda){
            //particle is not overlapping but is in the shoulder potential, so update energy
            *enew += 1.0;
        }
    }

    return 1;
}

// Return 0 if overlap else 1 if no overlap 
particle_overlap_energy check_all_overlap(double dist[][N], int n_particles, double lambda){

    particle_overlap_energy check;
    double enew = 0;
    for(int i = 0; i < n_particles; ++i){
        if(check_overlap(dist, n_particles, lambda, i, i + 1, &enew) == 0){
            check.no_overlap = 0;
            return check;
        }
    }
    check.no_overlap = 1;
    check.energy = enew;
    return check;
}


int change_volume(double r[][NDIM], double dist[][N], int n_particles, double box[], double lambda, double *energy, double beta, double P, double deltaV, gsl_rng *RNG){
	double old_box[NDIM]; // old box dimensions

    // Change volume by random deltaV
    double Vold = 1;
    for(int d = 0; d < NDIM; d++){
        Vold *= box[d];
    }
    double Vnew = Vold + (2 * gsl_rng_uniform(RNG) - 1) * deltaV;

    // Check if Vnew is non-negative, repeat above if necessary 
    while(Vnew < 0){
        Vnew = Vold + (2 * gsl_rng_uniform(RNG) - 1) * deltaV;
    }
    
    // Determine new box dimensions
    for(int d = 0; d < NDIM; d++){
        old_box[d] = box[d];
    }
    double Lnew = pow(Vnew, 1./NDIM);
    for(int d = 0; d < NDIM; d++){
    	box[d] = Lnew;
    }

    // Update particle positions to new scale 
    double scale_factor = box[0] / old_box[0];
    for(int i = 0; i < n_particles; i++){
    	for(int d = 0; d < NDIM; d++){
    		r[i][d] *= scale_factor;
    	}
    }

    // Update distances after volume change 
    update_distance_volume_change(dist, n_particles, scale_factor);

    // Compute acceptance rule 
    particle_overlap_energy check = check_all_overlap(dist, n_particles, lambda);
    double enew = check.energy;
    double arg = -beta * ( (enew - *energy) + P * (Vnew - Vold) ) + n_particles * log(Vnew / Vold);

    // If rule is rejected or particles overlap, reset particle positions to old scale
    if(check.no_overlap == 0 || gsl_rng_uniform(RNG) > exp(arg)){
        double inverse_scale_factor = 1 / scale_factor;
    	for(int i = 0; i < n_particles; i++){
    		for(int d = 0; d < NDIM; d++){
    			r[i][d] *= inverse_scale_factor;
    		}
    	}
        for(int d = 0; d < NDIM; d++){
            box[d] = old_box[d];
        }

        update_distance_volume_change(dist, n_particles, inverse_scale_factor);
    	return 0;
    }
    *energy = enew;
    return 1;
}


int move_particle(double r[][NDIM], double dist[][N], int n_particles, double box[], double lambda, double *energy, double beta, double delta, gsl_rng *RNG){


    // Randomly select a particle and get its current position
    double pos[NDIM];
    int i = (int)(gsl_rng_uniform(RNG) * n_particles);
    // Randomly displace the particle in all directions
    for(int d = 0; d < NDIM; d++){
        pos[d] = r[i][d];
    }

    double eold = 0;
    check_overlap(dist, n_particles, lambda, i, 0, &eold);

    // Randomly displace the particle in all directions
    for(int d = 0; d < NDIM; d++){
        double ds = (gsl_rng_uniform(RNG) * 2 - 1) * delta;
        r[i][d] += ds;

        // Apply periodic boundary conditions if necessary
        r[i][d] -= floor(r[i][d] / box[d]) * box[d];
    }

    // update distances after move particle 
    update_distance_particle_move(r, dist, n_particles, box, i);

    double enew = 0;
    int no_overlap = check_overlap(dist, n_particles, lambda, i, 0, &enew);
    double dE = enew - eold;

    // Check if the move is allowed, i.e. no overlaps
    if(no_overlap == 1){
        double arg = - beta * dE;

        // Check if the move gets accepted and update if necessary
        if(gsl_rng_uniform(RNG) < exp(arg)){
            *energy += dE;
            return 1;
        }
    }
    //reset positions
    for(int d = 0; d < NDIM; d++){
        r[i][d] = pos[d];
    }

    // Update distances after move particle 
    update_distance_particle_move(r, dist, n_particles, box, i);

    return 0;
}

// Calculate the complex exponetial exp(i * k * theta) in terms of x and y difference of two particles. 
// To speed up the calculation, sines and cosines are avoided by expanding in terms of sin(theta) and 
// cosine(theta) and noting that these are replaced by respectively, y/r and x/r. 
void calculate_bond_order_exp(double x, double y, double r, int k, complex_number *chi){

	double rn;
	switch(k){
		case 2:
			rn = r * r;
			(*chi).re += (x - y) * (x + y) / rn ;
			(*chi).im += 2 * x * y / rn ;
			break;
		case 3:
			rn = r * r * r;
			(*chi).re += ( x * x * x - 3 * x * y * y ) / rn;
			(*chi).im += ( 3 * x * x * y - y * y * y )/ rn;
			break;
		case 4:
			rn = r * r * r * r;
			(*chi).re += ( x * x * x * x - 6 * x * x * y * y + y * y * y * y) / rn;
			(*chi).im += 4 * x * (x - y) * y * (x + y) / rn;
			break;
		case 5:
			rn = r * r * r * r * r;
			(*chi).re += ( x * x * x * x * x - 10 * x * x * x * y * y + 5 * x * y * y * y * y) / rn;
			(*chi).im += ( 5 * x * x * x * x * y - 10 * x * x * y * y * y + y * y * y * y * y) / rn;
			break;
		case 6:
			rn = r * r * r * r * r * r;
			(*chi).re += ( x * x * x * x * x * x - 15 * x * x * x * x * y * y + 15 * x * x * y * y * y * y - y * y * y * y * y * y) / rn;
			(*chi).im += ( 6 * x * x * x * x * x * y - 20 * x * x * x * y * y * y + 6 * x * y * y * y * y * y) / rn;
			break;
	}
}

double calculate_bond_order(double r[][NDIM], int n_particles, double box[], nn_info nn[][N], int m_nn[], int k){

	assert(NDIM == 2);

	double chi_tot = 0;
	for(int i = 0; i < n_particles; i++){

		complex_number chi;
		chi.re = 0;
		chi.im = 0;

		for(int j = 1; j <= m_nn[i]; j++){

			double min_x = r[nn[i][j].index][0] - r[i][0];
            min_x -= (int)(2.0 * min_x / box[0]) * box[0];

            double min_y = r[nn[i][j].index][1] - r[i][1];
            min_y -= (int)(2.0 * min_y / box[1]) * box[1];

            calculate_bond_order_exp(min_x, min_y,  *(nn[i][j].dist), k, &chi);
		}

		chi_tot += (chi.re * chi.re + chi.im * chi.im) / (m_nn[i] * m_nn[i]);
	}

	return chi_tot / n_particles;
}

void read_data(double r[][NDIM], int *n_particles, double box[], const char *init_filename){
    // Initialise the file pointer
    FILE *fp;
    fp = fopen(init_filename, "r");

    // First read in the number of particles and box dimension
    fscanf(fp, "%d", n_particles);
    for(int d = 0; d < NDIM; d++){
        double low, high;
        fscanf(fp, "%lf %lf", &low, &high);
        box[d] = high - low;
    }

    // Read in all the particle positions
    for(int i = 0; i < *n_particles; i++){
        for(int d = 0; d < NDIM; d++){
            fscanf(fp, "%lf", &r[i][d]);
        }
        fscanf(fp, "%*f"); // Ignore the DIAMETER field
    }
}

void write_data(int step, double r[][NDIM], int n_particles, double box[]){
    char buffer[128];
    sprintf(buffer, "coords_step%07d.dat", step);
    FILE* fp = fopen(buffer, "w");
    int d, n;
    fprintf(fp, "%d\n", n_particles);
    for(d = 0; d < NDIM; ++d){
        fprintf(fp, "%lf %lf\n",0.0,box[d]);
    }
    for(n = 0; n < n_particles; ++n){
        for(d = 0; d < NDIM; ++d) fprintf(fp, "%lf\t", r[n][d]);
        fprintf(fp, "%lf\n", DIAMETER);
    }
    fclose(fp);
}

int main(int argc, char* argv[]){

    // Initialization variables 
    const int mc_steps = 10000;
    const int output_steps = 100;
    const int some_steps = 50000;
    const int mean_steps = 5000;
    const double tolerance = 0.02;
    const double beta = 1/ .3; // Reduced temperature \beta 
    const double P = 10 * .3; // Reduced pressure P 
    const char* init_filename = "xy.dat";

    const double lambda = 1.4; // the hard-core shoulder potential width
    const double alpha = .05; // learning rate 

    // Simulation variables 
    int n_particles = 0;
    double radius = 0.5 * DIAMETER;
    double particle_volume;
    double r[N][NDIM];
    double dist[N][N];
    double box[NDIM];
    double energy;
    double delta  = .4; // displacement magnitude,  tune to 50% acceptence rate 
    double deltaV = 3.; // volume change magnitude, tune to 30% acceptence rate 

    int     m_nn[N];
    nn_info nn[N][N];


    ////////////////////////////////
    //  INITIALIZE THE SIMULATION //
    ////////////////////////////////

    // set random seed
    gsl_rng *RNG = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(RNG, time(NULL));

    // computing particle volume
    if(NDIM == 3) particle_volume = M_PI * pow(DIAMETER, 3.0) / 6.0;
    else if(NDIM == 2) particle_volume = M_PI * pow(radius, 2.0);
    else{
        printf("Number of dimensions NDIM = %d, not supported.", NDIM);
        return 0;
    }

    // import the starting positions
    read_data(r, &n_particles, box, init_filename);
    if(n_particles == 0){
        printf("Error: Number of particles, n_particles = 0.\n");
        return 0;
    }

    // initialize the distance array
    initialise_sann(r, dist, nn, n_particles, box);

    // initialise energy 
    particle_overlap_energy check = check_all_overlap(dist, n_particles, lambda);

    assert(check.no_overlap == 1);
    energy = check.energy;


    ///////////////////////////
    //  START THE SIMULATION //
    ///////////////////////////

    // phase 0: volume convergence,
	// phase 1: delta and deltaV convergence,
	// phase 2: measurements 

	printf("#Step \t Volume \t Volume-acc \t deltaV \t Move-acc \t delta\n");

    // start the first phase, converging the volume 
    int move_accepted = 0;
    int vol_accepted = 0;
   
    int converged = 0;
    double old_avg = -1;
    double curr_avg;

    double volumes[mean_steps];

    int step_tot = 0;
    for(int step = 0; converged < 5; ++step){
        for(int n = 0; n < n_particles; ++n){
            move_accepted += move_particle(r, dist, n_particles, box, lambda, &energy, beta, delta, RNG);
        }
        vol_accepted += change_volume(r, dist, n_particles, box, lambda, &energy, beta, P, deltaV, RNG);
        
        double volume = 1;
        for(int d = 0; d < NDIM; d++){
        	volume *= box[d];
        }

        volumes[step % mean_steps] = volume;

        if(step % output_steps == 0){
        	double mov_acc_ratio = (double)move_accepted / (n_particles * output_steps);
        	double vol_acc_ratio = (double)vol_accepted /  output_steps;

            printf("%d \t %lf \t %lf \t %lf \t %lf \t %lf \n", step, box[0] * box[1], vol_acc_ratio, deltaV, mov_acc_ratio, delta );

            move_accepted = 0;
            vol_accepted = 0;

            
            // update deltaV to increase or decrease the vol_acc_ratio
            if( .7 < vol_acc_ratio || vol_acc_ratio < .5){
            	deltaV *= 1 + alpha * (vol_acc_ratio - 0.6);
           	}

            // update delta to increase or decrease the mov_acc_ratio
            if( .7 < mov_acc_ratio || mov_acc_ratio < .3){
            	delta  *= 1 + alpha * (mov_acc_ratio - 0.5);
               	// make sure delta does not exceeds half the box length, that be wierd
               	if(box[0]/2 < delta){
                   	delta = box[0]/2;
               	}
            }
        }

        // Calculate mean volume every mean_steps steps and check for convergence
        if(step % mean_steps == 0 && step > 0){
            curr_avg = mean(volumes, mean_steps);
            printf("deviation: %lf\n", fabs((curr_avg - old_avg) / old_avg));
            if(fabs((curr_avg - old_avg) / old_avg) < tolerance){
                converged++;
            }
            else{
                old_avg = curr_avg;
                converged = 0;
            }
        }

        step_tot++;
        if(step_tot % 100 == 0){
       // write_data(step_tot, r, n_particles, box);
        }
    }
    printf("End of phase 0\n");

    // volume is converged, now start second phase, converging the delta and deltaV magnitude
    int times_in_bounds = 0;
    for(int step = 0; times_in_bounds < 10; ++step){
        for(int n = 0; n < n_particles; ++n){
            move_accepted += move_particle(r, dist, n_particles, box, lambda, &energy, beta, delta, RNG);
        }
        vol_accepted += change_volume(r, dist, n_particles, box, lambda, &energy, beta, P, deltaV, RNG);


        if(step % output_steps == 0){
        	double mov_acc_ratio = (double)move_accepted / (n_particles * output_steps);
        	double vol_acc_ratio = (double)vol_accepted /  output_steps;

            printf("%d \t %lf \t %lf \t %lf \t %lf \t %lf \n", step, box[0] * box[1], vol_acc_ratio, deltaV, mov_acc_ratio, delta );

            move_accepted = 0;
            vol_accepted = 0;

            times_in_bounds += 1;

            // update deltaV to increase or decrease the vol_acc_ratio
            if( .4 < vol_acc_ratio || vol_acc_ratio < .2){
            	deltaV *= 1 + alpha * (vol_acc_ratio - 0.3);
            	times_in_bounds = 0;
            }

            // update delta to increase or decrease the mov_acc_ratio
           if( .55 < mov_acc_ratio || mov_acc_ratio < .45){
            	delta  *= 1 + alpha * (mov_acc_ratio - 0.5);
            	times_in_bounds = 0;
                // make sure delta does not exceeds half the box length, that be wierd
                if(box[0]/2 < delta){
                    delta = box[0]/2;
                }
            }
        }

        step_tot++;
        if(step_tot % 100 == 0){
        //write_data(step_tot, r, n_particles, box);
        }
    }
	printf("End of phase 1\n");


	for(int step = 0; step < some_steps; step++){

    	for(int n = 0; n < n_particles; ++n){
            move_accepted += move_particle(r, dist, n_particles, box, lambda, &energy, beta, delta, RNG);
        }
        vol_accepted += change_volume(r, dist, n_particles, box, lambda, &energy, beta, P, deltaV, RNG);

        if(step % output_steps == 0){
        	double mov_acc_ratio = (double)move_accepted / (n_particles * output_steps);
        	double vol_acc_ratio = (double)vol_accepted /  output_steps;

            printf("%d \t %lf \t %lf \t %lf \t %lf \t %lf \n", step, box[0] * box[1], vol_acc_ratio, deltaV, mov_acc_ratio, delta );

            move_accepted = 0;
            vol_accepted = 0;

        }

        step_tot++;
        if(step_tot % 100 == 0){
        //write_data(step_tot, r, n_particles, box);
        }
    }

	double bond_order[5][mc_steps];

    // delta and deltaV are converged, now start last phase, measuring
    for(int step = 0; step < mc_steps; step++){

    	for(int n = 0; n < n_particles; ++n){
            move_accepted += move_particle(r, dist, n_particles, box, lambda, &energy, beta, delta, RNG);
        }
        vol_accepted += change_volume(r, dist, n_particles, box, lambda, &energy, beta, P, deltaV, RNG);
        
        // calculate bond order parameters
        sann(r, dist, nn, m_nn, n_particles);
        for(int k = 2; k <= 6; k++){
        	bond_order[k - 2][step] = calculate_bond_order(r, n_particles, box, nn, m_nn, k);
        }

        if(step % output_steps == 0){
        	double mov_acc_ratio = (double)move_accepted / (n_particles * output_steps);
        	double vol_acc_ratio = (double)vol_accepted /  output_steps;

            printf("%d \t %lf \t %lf \t %lf \t %lf \t %lf \n", step, box[0] * box[1], vol_acc_ratio, deltaV, mov_acc_ratio, delta );

            move_accepted = 0;
            vol_accepted = 0;

        }

        step_tot++;
        if(step_tot % 100 == 0){
        //write_data(step_tot, r, n_particles, box);
        }
    }
    printf("End of phase 2\n");

    for( int k = 0; k < 5; k++){
    	double mean_bond_order = mean( bond_order[k], mc_steps);
    	printf("bond order %d: \t %lf\n", k + 2, mean_bond_order);
    }
    
    write_data(step_tot, r, n_particles, box);

    return 0;
}
